<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajout du champ de profil sur les offres
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function abonnements_profils_declarer_tables_objets_sql($tables) {
	$tables['spip_abonnements_offres']['field']['id_profil'] = 'bigint(21) NOT NULL DEFAULT 0';
	$tables['spip_abonnements_offres']['champs_editables'][] = 'id_profil';
	$tables['spip_abonnements_offres']['champs_versionnes'][] = 'id_profil';
	
	return $tables;
}

function abonnements_profils_formulaire_saisies($flux) {
	// Ajouter une configuration
	if ($flux['args']['form'] == 'configurer_abonnements') {
		include_spip('inc/saisies');
		
		$flux['data'][] = array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'forcer_profil',
				'label_case' => _T('abonnements:cfg_forcer_profil_label_case'),
				'defaut' => lire_config('abonnements/forcer_profil'),
			),
		);
	}
	// Ajouter le champ de profil pour les offres
	elseif ($flux['args']['form'] == 'editer_abonnements_offre') {
		include_spip('inc/saisies');
		include_spip('inc/config');
		
		$saisie_profil = array(
			'saisie' => 'profils',
			'options' => array(
				'nom' => 'id_profil',
				'label' => _T('abonnements_offre:champ_id_profil_label'),
				'explication' => _T('abonnements_offre:champ_id_profil_explication'),
				'option_intro' => _T('profil:info_aucun_profil'),
				'obligatoire' => lire_config('abonnements/forcer_profil'),
				'cacher_option_intro' => lire_config('abonnements/forcer_profil'),
				'defaut' => lire_config('abonnements/forcer_profil') ? lire_config('profils/id_profil_defaut') : null,
			),
		);
		$chemin_taxe = saisies_chercher($flux['data'], 'quota', true);
		$flux['data'] = saisies_inserer($flux['data'], $saisie_profil);
	}
	
	return $flux;
}

function abonnements_profils_recuperer_fond($flux) {
	if (
		$flux['args']['fond'] == 'prive/objets/contenu/abonnements_offre'
		and $id_abonnements_offre = $flux['args']['contexte']['id_abonnements_offre']
	) {
		if ($id_profil = sql_getfetsel('id_profil', 'spip_abonnements_offres', 'id_abonnements_offre = '.$id_abonnements_offre)) {
			$titre_profil = '<a href="'.generer_url_entite($id_profil, 'profil').'">' . sql_getfetsel('titre', 'spip_profils', 'id_profil = '.$id_profil) . '</a>';
		}
		else {
			$titre_profil = _T('profil:info_aucun_profil');
		}
		
		$profil = '
<div class="champ contenu_id_profil">
	<div class="label">' . _T('abonnements_offre:champ_id_profil_label') . ' : </div>
	<span class="id_profil">' . $titre_profil . '</span>
</div>';

		$flux['data']['texte'] = $profil . $flux['data']['texte'];
	}
	
	return $flux;
}
