<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'champ_id_profil_explication' => 'Assigner un <a href="?exec=profils">profil d’informations</a> à remplir pour ces abonnements.',
	'champ_id_profil_label' => 'Profil',
);
