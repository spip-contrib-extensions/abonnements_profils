<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Billetteries nominatives par Profils
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin Abonnements par Profils.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function abonnements_profils_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();
	
	$maj['create'] = array(
		array('maj_tables', array('spip_abonnements_offres'))
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugins Abonnements par Profils.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function abonnements_profils_vider_tables($nom_meta_base_version) {
	sql_alter('table spip_abonnements_offres drop id_profil');
	
	effacer_meta($nom_meta_base_version);
}
