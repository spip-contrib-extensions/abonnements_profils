<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'champ_id_profil_explication' => 'Assign a <a href="?exec=profils">information profile</a> to be completed for those subscriptions.',
	'champ_id_profil_label' => 'Profile',
);
