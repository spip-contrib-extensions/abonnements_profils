<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_forcer_profil_label_case' => 'Forcer au choix d’un profil lors de l’édition d’un type de billet',
);
